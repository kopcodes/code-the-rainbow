const chroma = require("chroma-js");

const generateSpectrum = (start, end, trim = false) => {
  const spectrum = chroma.scale([start, end]).correctLightness().colors(8);
  return trim ? spectrum.slice(1, -1) : spectrum
}

const colors = {
  colors: {
    // colors colors
    rose: "#d47479",
    yellow: "#fdc725",
    black: "black",
    white: "white",
    offBlack: chroma("black").brighten(0.4).hex(),
    offWhite: chroma("white").darken(0.4).hex(),
    // spectrum colors
    roseSpectrum: (get) => generateSpectrum(get.colors.rose, get.colors.white, true),
    // logical colors
    accent: (get) => get.colors.yellow,
    highlight: (get) => chroma(get.colors.accent(get)).brighten(1).hex(),
    background: (get) => get.colors.rose,
    text: (get) => get.colors.yellow,
    box: (get) => chroma(get.colors.background(get)).darken(0.2).hex(),
  },
  // add additional color scheme here
  otherTheme: {
    highlight: (get) => chroma(get.colors.accent(get)).darken(1).hex(),
    background: (get) => get.colors.offBlack,
    text: (get) => get.colors.rose,
    box: (get) => chroma(get.otherTheme.background(get)).brighten(0.2).hex(),
  },
};

module.exports = colors;
