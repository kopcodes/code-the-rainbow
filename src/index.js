import feather from "feather-icons";
import '../node_modules/sanitize.css';
import './themeSwitch';
import "./style.scss";

document.addEventListener("DOMContentLoaded", () => {
  feather.replace();
});
