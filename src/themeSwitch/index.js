
import './index.scss';

const switches = document.querySelectorAll('.themeSwitch');
const targetEl = document.querySelector('html');
const themeFromStorage = localStorage.getItem('theme');

const checkForDarkMode = () => {
  return window.matchMedia('(prefers-color-scheme: dark)').matches;
};

const saveState = (state) => {
  localStorage.setItem('theme', state);
};

const setTheme = (theme) => {
  targetEl.setAttribute('data-theme', theme);
};

const toggleState = (a, b) => {
  const theme = targetEl.getAttribute('data-theme') === a ? b : a;
  targetEl.setAttribute('data-theme', theme);

  saveState(theme);
};

// apply theme from localStorage or form machine settings
document.addEventListener('DOMContentLoaded', () => {
  if (themeFromStorage === null && checkForDarkMode()) {
    setTheme('dark');
  } else {
    themeFromStorage && setTheme(themeFromStorage);
  }
});

// switch theme on user interaction
switches.forEach((button) => {
  button.addEventListener('click', (e) => {
    toggleState('dark', 'default');
    e.preventDefault();
  });
});
