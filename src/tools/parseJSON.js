const fs = require("fs");

class ParseJSON {
  constructor(array) {
    this.array = array;
    this.tabWidth = 2;
    this.spacing = " ".repeat(this.tabWidth);
    this.convert();
  }

  callFunctionForJSON(func, context) {
    const returnValue = func(context);
    switch (typeof returnValue) {
      case "object":
        if (Array.isArray(returnValue)) {
          return returnValue;
        } else {
          return this.iterateForJSON(returnValue, context);
        }
      default:
        return returnValue;
    }
  }

  iterateForJSON(object, parentObj) {
    let newObject = {};
    for (let [key, value] of Object.entries(object)) {
      switch (typeof value) {
        case "object":
          if (Array.isArray(value)) {
            newObject[key] = value;
          } else {
            newObject[key] = this.iterateForJSON(value, parentObj);
          }
          break;
        case "function":
          newObject[key] = this.callFunctionForJSON(value, parentObj);
          break;
        default:
          newObject[key] = value;
      }
    }
    return newObject;
  }

  convert() {
    for (var i = 0, n = this.array.length; i < n; ++i) {
      const { map, distJSON } = this.array[i];

      let jsonMap = {};

      for (const [name, value] of Object.entries(map)) {
        const jsonItem = { [name]: this.iterateForJSON(value, map) };

        jsonMap = { ...jsonMap, ...jsonItem };
      }

      // output files with parsed content
      distJSON &&
        fs.writeFile(distJSON, JSON.stringify(jsonMap), (err) => {
          if (err) return console.error(err);
        });
    }
  }
}

module.exports = ParseJSON;
