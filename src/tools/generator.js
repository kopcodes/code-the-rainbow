const path = require("path");
const fs = require("fs");
const glob = require("glob");
const ParseJSON = require("./parseJSON");
const ParseSCSS = require("./parseSCSS");

// paths
const outputPath = path.resolve("./src/generated");
const sassOutputDir = outputPath;
const jsonOutputDir = outputPath;

// generate output path if not already there
!fs.existsSync(outputPath) && fs.mkdirSync(outputPath);
const generateStyles = (err, files) => {
  const allPaths = [];
  files.forEach((file) => {
    const vars = require(`../.${file}`);
    const filename = path.basename(file, ".js");
    allPaths.push({
      map: vars,
      distSCSS: path.resolve(sassOutputDir, `${filename}.scss`),
      distJSON: path.resolve(jsonOutputDir, `${filename}.json`)
    });
  });

  new ParseJSON(allPaths);
  new ParseSCSS(allPaths);
};

glob("./src/vars/*.js", function (err, files) {
  generateStyles(err, files);
  console.log("Successfully converted objects to SCSS maps and JSON 🧙‍♂️");
});
