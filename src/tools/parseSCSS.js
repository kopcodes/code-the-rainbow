const fs = require("fs");

class ParseSCSS {
  constructor(array) {
    this.array = array;
    this.tabWidth = 2;
    this.spacing = " ".repeat(this.tabWidth);
    this.convert();
  }

  callFunctionForSCSS(func, context, N) {
    const returnValue = func(context);
    switch (typeof returnValue) {
      case "object":
        if (Array.isArray(returnValue)) {
          let array = "";
          returnValue.forEach((val) => {
            array += `${N}${this.iterateForSCSS(val, context, N)}`;
          });
          return `(\n${array}${N}),\n`;
        } else {
          return `(\n${N}${this.iterateForSCSS(
            returnValue,
            context,
            N
          )}${N}),\n`;
        }
      default:
        return `${returnValue},\n`;
    }
  }

  iterateForSCSS(object, data, nesting = "") {
    const CN = nesting.length ? nesting + this.spacing : this.spacing;
    let mapString = "";

    switch (typeof object) {
      case "object":
        break;
      case "function":
        return `${this.spacing}${this.callFunctionForSCSS(
          object,
          data,
          CN
        )},\n`;
      default:
        return `${this.spacing}${object},\n`;
    }

    for (const [key, value] of Object.entries(object)) {
      switch (typeof value) {
        case "object":
          if (Array.isArray(value)) {
            let array = "";
            // console.log(typeof value, value);
            value.forEach((val) => {
              array += `${CN}${this.iterateForSCSS(val, data, CN)}`;
            });

            mapString += `${CN}${key}: (\n${array}${CN}),\n`;
          } else {
            mapString += `${CN}${key}: (\n${this.iterateForSCSS(
              value,
              data,
              CN
            )}${CN}),\n`;
          }
          break;

        case "function":
          mapString += `${CN}${key}: ${this.callFunctionForSCSS(
            value,
            data,
            CN
          )}`;
          break;

        default:
          mapString += `${CN}${key}: ${value},\n`;
          break;
      }
    }
    return mapString;
  }

  convert() {
    for (var i = 0, n = this.array.length; i < n; ++i) {
      const { map, distSCSS } = this.array[i];

      let sassMap = "";

      for (const [name, value] of Object.entries(map)) {
        const sassItem = `$${name}: (\n${this.iterateForSCSS(
          value,
          map
        )}) !default;\n\n`;

        sassMap += sassItem;
      }

      // output files with parsed content
      distSCSS &&
        fs.writeFile(distSCSS, sassMap, (err) => {
          if (err) return console.error(err);
        });
    }
  }
}

module.exports = ParseSCSS;
